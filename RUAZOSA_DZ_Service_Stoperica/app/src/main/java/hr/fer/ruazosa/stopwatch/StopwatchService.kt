package hr.fer.ruazosa.stopwatch

import android.app.Service
import android.content.Intent
import android.os.IBinder

class StopwatchService: Service() {

    private var clockThread: Thread? = null
    private var loop_count: Int = 0

    override fun onBind(intent: Intent?): IBinder? {
        // TODO("Not yet implemented")
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        val command = intent?.getStringExtra(STOPWATCH_ACTION)
        if(command != null){
            startClockThread()
        }
        return super.onStartCommand(intent, flags, startId)
    }

    private fun startClockThread(){
        clockThread = Thread(Runnable {
                // 980 msec sleep, 20 msec left for "homework"
                Thread.sleep(980)
                //send broadcast
                val secondPassedIntent = Intent()
                secondPassedIntent.setAction(SECOND_PASSED)
                sendBroadcast(secondPassedIntent)
                stopSelf()
        })
        // start the thread
        clockThread?.run()
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    companion object{
        val STOPWATCH_ACTION_ID = "StopWatch"
        val STOPWATCH_ACTION = "StopwatchServiceForever"
        val SECONDS_TO_SLEEP = "secondsToSleep"
        val WAIT_TIME_PASSED = "hr.fer.ruazosa.startedservicedemo.waittimepassed"
        val SECOND_PASSED = "hr.fer.ruazosa.startedservicedemo.waittimepassed"
    }
}