package hr.fer.ruazosa.stopwatch

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import java.lang.StringBuilder

class MainActivity : AppCompatActivity() {

    @Volatile private var counter_sec_val: Long = 0
    private lateinit var stopwatchServiceBroadCastReceiver: StopwatchServiceBroadCastReceiver
    private lateinit var broadcastIntentFilter: IntentFilter
    private lateinit var stopwatchServiceIntent: Intent
    private var isPlaying: Boolean = false

    // funkcija koja sluzi za pretvorbu brojcane vrijednosti
    // u String prikaz unutar TextViewa
    fun setStopWatchTimePassedTextViewString(counter_sec: Long){
        val minutes: Long = counter_sec / 60
        val seconds: Long = counter_sec % 60
        val text_builder = StringBuilder()

        if(minutes < 10){
            text_builder.append("0")
        }
        text_builder.append(minutes.toString())
        text_builder.append(":")
        if(seconds < 10){
            text_builder.append("0")
        }
        text_builder.append(seconds.toString())

        stopwatchTimePassedTextView.text = text_builder.toString()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // na pocetku omoguci akciju klikanja samo na play gumbu
        playFloatingActionButton.isEnabled = true
        pauseFloatingActionButton.isEnabled = false
        stopFloatingActionButton.isEnabled = false
        isPlaying = false
        stopwatchStateTextView.text = getString(R.string.stopwatch_halted_string)
        setStopWatchTimePassedTextViewString(counter_sec_val)

        //initialize variables
        stopwatchServiceBroadCastReceiver = StopwatchServiceBroadCastReceiver()
        broadcastIntentFilter = IntentFilter()
        broadcastIntentFilter?.addAction(StopwatchService.SECOND_PASSED)
        registerReceiver(stopwatchServiceBroadCastReceiver, broadcastIntentFilter)
        stopwatchServiceIntent = Intent(applicationContext, StopwatchService::class.java)



        playFloatingActionButton.setOnClickListener {
            // Toast je za provjeru ispravnosti isEnabled postavljanja, koristeno za debug
            //Toast.makeText(applicationContext, "Play button click listener called!", Toast.LENGTH_LONG).show()
            //Log.d("StopWatch_Debug", "MainThread --> counter_msec_last_val: " + counter_msec_last_val.toString())
            playFloatingActionButton.isEnabled = false
            pauseFloatingActionButton.isEnabled = true
            stopFloatingActionButton.isEnabled = true
            isPlaying = true
            stopwatchStateTextView.text = getString(R.string.stopwatch_running_string)
            // start stopwatch service
            stopwatchServiceIntent = Intent(applicationContext, StopwatchService::class.java)
            stopwatchServiceIntent!!.putExtra(StopwatchService.STOPWATCH_ACTION, StopwatchService.STOPWATCH_ACTION)
            startService(stopwatchServiceIntent)
        }

        pauseFloatingActionButton.setOnClickListener {
            // pauziraj brojac
            // Toast je za provjeru ispravnosti isEnabled postavljanja, koristeno za debug
            //Toast.makeText(applicationContext, "Pause button click listener called!", Toast.LENGTH_LONG).show()
            playFloatingActionButton.isEnabled = true
            pauseFloatingActionButton.isEnabled = false
            stopFloatingActionButton.isEnabled = true
            isPlaying = false
            stopwatchStateTextView.text = getString(R.string.stopwatch_paused_string)
        }

        stopFloatingActionButton.setOnClickListener {
            // zaustavi brojac
            // Toast je za provjeru ispravnosti isEnabled postavljanja
            //Toast.makeText(applicationContext, "Stop button click listener called!", Toast.LENGTH_LONG).show()
            playFloatingActionButton.isEnabled = true
            pauseFloatingActionButton.isEnabled = false
            stopFloatingActionButton.isEnabled = false
            isPlaying = false
            stopwatchStateTextView.text = getString(R.string.stopwatch_halted_string)
            counter_sec_val = 0
            setStopWatchTimePassedTextViewString(counter_sec_val)
            //stopwatchServiceIntent = Intent(applicationContext, StopwatchService::class.java)
            //stopService(stopwatchServiceIntent)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    inner class StopwatchServiceBroadCastReceiver : BroadcastReceiver() {

        override fun onReceive(context: Context, intent: Intent) {
            // do update clock stuff
            //Log.d("STOPWATCH_DEBUG", "Second passed broadcast routine was called!")
            if(isPlaying){
                // update UI
                counter_sec_val++
                setStopWatchTimePassedTextViewString(counter_sec_val)
                // first stop the service
                stopwatchServiceIntent = Intent(applicationContext, StopwatchService::class.java)
                stopService(stopwatchServiceIntent)
                // then start it again
                stopwatchServiceIntent = Intent(applicationContext, StopwatchService::class.java)
                stopwatchServiceIntent!!.putExtra(StopwatchService.STOPWATCH_ACTION, StopwatchService.STOPWATCH_ACTION)
                startService(stopwatchServiceIntent)
            }else{
                // stop the service, but don't update UI
                stopwatchServiceIntent = Intent(applicationContext, StopwatchService::class.java)
                stopService(stopwatchServiceIntent)
            }
        }
    }

}
